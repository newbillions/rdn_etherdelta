STAT 2017-11-08

------------------BUY------------------
	total_token_buy 413293.2793888569 RDN
	total_eth_spent 1373.949805052621 ETH
	avg_buy_price 0.0033243942584411284 RDN/ETH

0xc0ad7700cefe80f198a696e1a78931550add582e 91 ETH 0.00294 RDN/ETH
0xb447f0503432e79a35dfcd63ec9d006d5ee82252 76 ETH 0.00331 RDN/ETH
0x337ceb394deeaaa0dd13b45d1101a2a6e53634af 68 ETH 0.00355 RDN/ETH
0xb7938b1faab8845cc5788f3cc6251b03660ba194 57 ETH 0.00361 RDN/ETH
0x3ab8e2d298c3101cba08c27d14648515452713cc 52 ETH 0.00371 RDN/ETH
0x8af55e3395b878d7e02971fb35f6f214a97c83e4 49 ETH 0.00312 RDN/ETH
0x5e0dd4b7c50e12306d2dcd3a12a120d22fa08524 38 ETH 0.00341 RDN/ETH
0xfe7432ca250c08d8fe7e6cde61c0b1a9ba0fc2ee 37 ETH 0.00299 RDN/ETH
0xb5909851f326c11ca574b63f3111956120217940 34 ETH 0.00344 RDN/ETH
0x61c85a10f0b86a5c069a627bd9e6f821a355a05b 33 ETH 0.00542 RDN/ETH
------------------SELL------------------
	total_token_sell 369011.5546432362 RDN
	total_eth_get 1022.5543753146467 ETH
	avg_sell_price 0.0027710632972001697 RDN/ETH

0x4132d03d9e92339b789027733cd5265285330fa0 133 ETH 0.00335 RDN/ETH
0xe897b1a69bb05cbab34310c91412c4deccb8fd61 106 ETH 0.00331 RDN/ETH
0x095718762a36548710afde4e2fe81bbb9ff58483 54 ETH 0.00365 RDN/ETH
0xf74fccfc0497132279917ef932dacc650fdfa0bd 52 ETH 0.00339 RDN/ETH
0x8af55e3395b878d7e02971fb35f6f214a97c83e4 50 ETH 0.00355 RDN/ETH
0x88844fb636e11b70c4c2fa916237b1ba14b21be6 49 ETH 0.003 RDN/ETH
0x76e2e2d4d655b83545d4c50d9521f5bc63bc5329 48 ETH 0.003 RDN/ETH
0x549f2ac2f129c6df05e3b13122e86b45d9cd4fc3 46 ETH 0.00296 RDN/ETH
0xee608ce732db7f5f8c24878229c197aac55a422c 45 ETH 0.00377 RDN/ETH
0x65a0f492142938a6a24d88513375e1ffee04441a 44 ETH 0.00312 RDN/ETH


	2017-11-08 Hourly 13:00-14:00

**************BUY------------------
	total_token_buy 364.4557637826361 RDN
	total_eth_spent 1.4428999992610885 ETH
	avg_buy_price 0.00395905386235473 RDN/ETH

**************SELL------------------
	total_token_sell 0.001300021 RDN
	total_eth_get 2.401042 ETH
	tavg_sell_price 1846.9255496641977 RDN/ETH



	2017-11-08 Hourly 14:00-15:00

**************BUY------------------
	total_token_buy 11577.363708809666 RDN
	total_eth_spent 56.76568153038157 ETH
	avg_buy_price 0.004903161285948575 RDN/ETH

**************SELL------------------
	total_token_sell 1730.8454790150533 RDN
	total_eth_get 6.681660058817665 ETH
	tavg_sell_price 0.0038603446349352332 RDN/ETH



	2017-11-08 Hourly 15:00-16:00

**************BUY------------------
	total_token_buy 44189.76489011517 RDN
	total_eth_spent 129.48932205941477 ETH
	avg_buy_price 0.0029303012220456576 RDN/ETH

**************SELL------------------
	total_token_sell 100141.84928449495 RDN
	total_eth_get 272.4480857820352 ETH
	tavg_sell_price 0.002720621675439926 RDN/ETH



	2017-11-08 Hourly 16:00-17:00

**************BUY------------------
	total_token_buy 50269.81913731278 RDN
	total_eth_spent 151.10212635056436 ETH
	avg_buy_price 0.0030058219612413284 RDN/ETH

**************SELL------------------
	total_token_sell 33460.201653934426 RDN
	total_eth_get 95.1622496071216 ETH
	tavg_sell_price 0.0028440429197452823 RDN/ETH



	2017-11-08 Hourly 17:00-18:00

**************BUY------------------
	total_token_buy 65087.053061877945 RDN
	total_eth_spent 234.39221476742205 ETH
	avg_buy_price 0.0036012110510609 RDN/ETH

**************SELL------------------
	total_token_sell 22751.01269206503 RDN
	total_eth_get 79.8605014906245 ETH
	tavg_sell_price 0.00351019546125425 RDN/ETH



	2017-11-08 Hourly 18:00-19:00

**************BUY------------------
	total_token_buy 18527.722012743256 RDN
	total_eth_spent 56.20809695699992 ETH
	avg_buy_price 0.003033729506430436 RDN/ETH

**************SELL------------------
	total_token_sell 94231.11848831602 RDN
	total_eth_get 280.0027337978477 ETH
	tavg_sell_price 0.002971446569771599 RDN/ETH



	2017-11-08 Hourly 19:00-20:00

**************BUY------------------
	total_token_buy 24627.52489928155 RDN
	total_eth_spent 71.57598034755027 ETH
	avg_buy_price 0.0029063407971475982 RDN/ETH

**************SELL------------------
	total_token_sell 66428.12406772716 RDN
	total_eth_get 117.13380060732958 ETH
	tavg_sell_price 0.0017633164002630146 RDN/ETH



	2017-11-08 Hourly 20:00-21:00

**************BUY------------------
	total_token_buy 39856.49402497292 RDN
	total_eth_spent 120.78947456477401 ETH
	avg_buy_price 0.003030609628862259 RDN/ETH

**************SELL------------------
	total_token_sell 7091.240878334484 RDN
	total_eth_get 20.35244686263686 ETH
	tavg_sell_price 0.002870082572546461 RDN/ETH



	2017-11-08 Hourly 21:00-22:00

**************BUY------------------
	total_token_buy 71632.98361506098 RDN
	total_eth_spent 238.08685191365782 ETH
	avg_buy_price 0.0033237042476560977 RDN/ETH

**************SELL------------------
	total_token_sell 8710.658944362754 RDN
	total_eth_get 28.481436545984312 ETH
	tavg_sell_price 0.0032697223858611226 RDN/ETH



	2017-11-08 Hourly 22:00-23:00

**************BUY------------------
	total_token_buy 26061.74841554635 RDN
	total_eth_spent 90.42901657901024 ETH
	avg_buy_price 0.0034697985391136514 RDN/ETH

**************SELL------------------
	total_token_sell 18810.556797251036 RDN
	total_eth_get 61.11043730866805 ETH
	tavg_sell_price 0.0032487309103789363 RDN/ETH


STAT 2017-11-09

------------------BUY------------------
	total_token_buy 183785.42526009204 RDN
	total_eth_spent 677.4081343128861 ETH
	avg_buy_price 0.003685864280882024 RDN/ETH

0x53819e5704484bb3ead4a587b47688bf0d455b71 39 ETH 0.00377 RDN/ETH
0x8d02909a554d55bd08815619a0f0a791d7657cc2 38 ETH 0.00375 RDN/ETH
0x9be5df7ce0a926effa4f6e7d9fc41f0c396655a3 36 ETH 0.00368 RDN/ETH
0x92700c377365002a4c5db2201e7c265b8b295d33 26 ETH 0.00368 RDN/ETH
0xb68012b4c455d4b0663a2b37214fb157872d6660 24 ETH 0.00372 RDN/ETH
0x8bd19845157c3dce37b4c984c6de4976995c4f26 23 ETH 0.00379 RDN/ETH
0x8af55e3395b878d7e02971fb35f6f214a97c83e4 20 ETH 0.00379 RDN/ETH
0x00077485e6ce60531f9766726141fa3debbcdcac 19 ETH 0.00376 RDN/ETH
0x5918c0ba4c600b617fe376d1408c9a5132707ba1 14 ETH 0.00365 RDN/ETH
0xd997e1beb7450d6d07085a6357451364b992844e 13 ETH 0.00378 RDN/ETH
------------------SELL------------------
	total_token_sell 555354.6194771352 RDN
	total_eth_get 1874.386042486265 ETH
	avg_sell_price 0.003375115604964255 RDN/ETH

0x4132d03d9e92339b789027733cd5265285330fa0 75 ETH 0.00379 RDN/ETH
0x095718762a36548710afde4e2fe81bbb9ff58483 61 ETH 0.00379 RDN/ETH
0x8bc73170f8ec0644d91c759057636ac12de6df22 57 ETH 0.00367 RDN/ETH
0xee608ce732db7f5f8c24878229c197aac55a422c 43 ETH 0.00379 RDN/ETH
0x8af55e3395b878d7e02971fb35f6f214a97c83e4 37 ETH 0.00371 RDN/ETH
0x61ea262d52fd1bf63186793740d7d93defe812da 32 ETH 0.00364 RDN/ETH
0x237e56cd98b289c657aef13a174d694058516f4b 31 ETH 0.00368 RDN/ETH
0x9234f3bf734a4abf9595832f4903952add72268f 23 ETH 0.00377 RDN/ETH
0xea65baf6c3c6abda333cb149e43d836268458d72 14 ETH 0.00375 RDN/ETH
0xa58fb1190daf13b0a6801ffc2638dd12d938aaf6 13 ETH 0.00375 RDN/ETH


	2017-11-09 Hourly 0:00-1:00

**************BUY------------------
	total_token_buy 20243.53721169497 RDN
	total_eth_spent 74.24777187503685 ETH
	avg_buy_price 0.003667727191083132 RDN/ETH

**************SELL------------------
	total_token_sell 10996.185456570303 RDN
	total_eth_get 39.1230133915147 ETH
	tavg_sell_price 0.0035578713678513308 RDN/ETH



	2017-11-09 Hourly 1:00-2:00

**************BUY------------------
	total_token_buy 23179.927324871584 RDN
	total_eth_spent 87.26156656277556 ETH
	avg_buy_price 0.003764531499162454 RDN/ETH

**************SELL------------------
	total_token_sell 97914.0 RDN
	total_eth_get 361.89315049999993 ETH
	tavg_sell_price 0.0036960307055170856 RDN/ETH



	2017-11-09 Hourly 2:00-3:00

**************BUY------------------
	total_token_buy 17920.62844464791 RDN
	total_eth_spent 66.49585419600368 ETH
	avg_buy_price 0.003710576021448791 RDN/ETH

**************SELL------------------
	total_token_sell 78601.9614760112 RDN
	total_eth_get 266.1197189752484 ETH
	tavg_sell_price 0.003385662570983886 RDN/ETH



	2017-11-09 Hourly 3:00-4:00

**************BUY------------------
	total_token_buy 6503.049398263826 RDN
	total_eth_spent 22.852950978192737 ETH
	avg_buy_price 0.003514189971291619 RDN/ETH

**************SELL------------------
	total_token_sell 9925.55139599531 RDN
	total_eth_get 32.87206167161105 ETH
	tavg_sell_price 0.003311862521297712 RDN/ETH



	2017-11-09 Hourly 4:00-5:00

**************BUY------------------
	total_token_buy 10364.509034873914 RDN
	total_eth_spent 35.79640463360822 ETH
	avg_buy_price 0.0034537482203124627 RDN/ETH

**************SELL------------------
	total_token_sell 16146.5777337564 RDN
	total_eth_get 54.08411416199863 ETH
	tavg_sell_price 0.003349571349037583 RDN/ETH



	2017-11-09 Hourly 5:00-6:00

**************BUY------------------
	total_token_buy 5729.552514545601 RDN
	total_eth_spent 19.744952475164144 ETH
	avg_buy_price 0.003446159612821015 RDN/ETH

**************SELL------------------
	total_token_sell 277793.47586320096 RDN
	total_eth_get 900.6763052467658 ETH
	tavg_sell_price 0.003242251469182461 RDN/ETH



	2017-11-09 Hourly 6:00-7:00

**************BUY------------------
	total_token_buy 721.9145739316854 RDN
	total_eth_spent 2.4319485578845477 ETH
	avg_buy_price 0.0033687483889398282 RDN/ETH

**************SELL------------------
	total_token_sell 22694.515725980196 RDN
	total_eth_get 70.43928780633922 ETH
	tavg_sell_price 0.0031038021985946955 RDN/ETH



	2017-11-09 Hourly 7:00-8:00

**************BUY------------------
	total_token_buy 44077.10098987059 RDN
	total_eth_spent 160.96058651541168 ETH
	avg_buy_price 0.0036517961231706734 RDN/ETH

**************SELL------------------
	total_token_sell 10195.269995040393 RDN
	total_eth_get 34.5540372511219 ETH
	tavg_sell_price 0.0033892223813524417 RDN/ETH



	2017-11-09 Hourly 8:00-9:00

**************BUY------------------
	total_token_buy 48179.76685845242 RDN
	total_eth_spent 181.79103261028533 ETH
	avg_buy_price 0.003773182073387157 RDN/ETH

**************SELL------------------
	total_token_sell 24260.57161225731 RDN
	total_eth_get 89.38446046884957 ETH
	tavg_sell_price 0.003684350966557166 RDN/ETH



	2017-11-09 Hourly 9:00-10:00

**************BUY------------------
	total_token_buy 6865.438908939651 RDN
	total_eth_spent 25.82506590852369 ETH
	avg_buy_price 0.003761604502065594 RDN/ETH

**************SELL------------------
	total_token_sell 6826.5102183231975 RDN
	total_eth_get 25.23989301281627 ETH
	tavg_sell_price 0.0036973346857475256 RDN/ETH