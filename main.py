from json import loads
import json, requests
from amazon_db import put_item
import sched, time

s = sched.scheduler(time.time, time.sleep)

def add_tx_from_a_url(url):
    print("url {}".format(url))
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    r = requests.get(url, headers=headers)
    while r.status_code != 200:
        print("\t\t r.status_code:{}".format(r.status_code))
        r = requests.get(url,headers=headers)
    s = r.content
    try:
        jsons = loads(s)
    except:
        return 0
    for json in jsons:
        txHash,date,price,side,amount,amountBase,buyer,seller,tokenAddr = json['txHash'],json["date"],json['price'],json['side'],json['amount'],json['amountBase'],json['buyer'],json['seller'],json['tokenAddr']
        put_item(txHash,date,price,side,amount,amountBase,buyer,seller,tokenAddr)
    print("\t\t len(jsons):{}".format(len(jsons)))
    return len(jsons)

def do_something(sc):
    print("Doing stuff...")
    url = 'https://api.etherdelta.com/trades/0x255aa6df07540cb5d3d297f0d0d4d84cb52bc8e6/'
    tx_count = add_tx_from_a_url(url+'0')
    page_number = 1
    while tx_count != 0:
        tx_count = add_tx_from_a_url(url+str(page_number))
        page_number += 1
    print("going to sleep")
    s.enter(10, 1, do_something, (sc,))

s.enter(1, 1, do_something, (s,))
s.run()
