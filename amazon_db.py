import boto3
import pandas as pd

# Get the service resource.
dynamodb = boto3.resource('dynamodb')

table_name = "rdn_etherdelta"
table = dynamodb.Table(table_name)

def exists(txHash):
    response = table.get_item(Key={'txHash': txHash})
    exist = "Item" in response
    return exist

def put_item(txHash,date,price,side,amount,amountBase,buyer,seller,tokenAddr):
    if exists(txHash): return
    item = {"txHash": txHash,
    "date": date,
    "price": price,
    "side": side,
    "amount": amount,
    "amountBase": amountBase,
    "buyer": buyer,
    "seller": seller,
    "tokenAddr": tokenAddr}

    res = table.put_item(Item=item)
    print(res)

def df_helper(m_dict):
    df = pd.DataFrame.from_dict(m_dict)
    df['date'] = pd.to_datetime(df['date'])
    df['price'] = df['price'].astype('float64')
    df['amount'] = df['amount'].astype('float64')
    df['amountBase'] = df['amountBase'].astype('float64')
    df = df.set_index('date')
    return df

def get_all_items():
    items = table.scan()
    datas = items['Items']
    buys = []
    sells = []
    for json in datas:
        if json['side'] == 'buy':
            buys.append(json)
        else:
            sells.append(json)

    df_buys = df_helper(buys)
    df_sells = df_helper(sells)
    assert(len(datas) == ((len(buys))+(len(sells))))
    return (df_buys,df_sells)
# put_item("123","123","123","123","123","123","123","123","123")
# table.delete_item()
# buys,sells = get_all_items()
