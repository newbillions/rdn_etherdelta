from amazon_db import get_all_items
import datetime
import operator

buys,sells = get_all_items()

today  = '2017-11-08'
tommrow = '2017-11-09'

def calculate_top_player(df,side,top_cut=10):
    top_dict = dict()
    for index, row in df.iterrows():
        if side == 'buyer':
            player = row.buyer
        else:
            player = row.seller
        if player not in top_dict:
            top_dict[player] = (row.amountBase,row.amount)
        else:
            (old_amountBase,oldAmount) = top_dict[player]
            top_dict[player] = (old_amountBase+row.amountBase,oldAmount+row.amount)

    sorted_top_dict = sorted(top_dict.items(), key=operator.itemgetter(1),reverse=True)[:top_cut]
    for address,(amountBase,amount) in sorted_top_dict:
        print("{} {} ETH {} RDN/ETH".format(address,int(amountBase),round(amountBase/amount,5)))

def calculate_stat_for_day(day,symbol='RDN'):
    today_buys = buys.ix[day:day]
    today_sells = sells.ix[day:day]
    print("\nSTAT {}\n".format(day))
    print("------------------BUY------------------")
    total_token_buy = today_buys['amount'].sum()
    total_eth_spent = today_buys['amountBase'].sum()
    avg_buy_price = total_eth_spent / total_token_buy


    print("\ttotal_token_buy {} {}".format(total_token_buy,symbol))
    print("\ttotal_eth_spent {} ETH".format(total_eth_spent,symbol))
    print("\tavg_buy_price {} {}/ETH\n".format(avg_buy_price,symbol))
    calculate_top_player(today_buys,'buyer')

    print("------------------SELL------------------")
    total_token_sell = today_sells['amount'].sum()
    total_eth_get = today_sells['amountBase'].sum()
    avg_sell_price = total_eth_get / total_token_sell

    print("\ttotal_token_sell {} {}".format(total_token_sell,symbol))
    print("\ttotal_eth_get {} ETH".format(total_eth_get,symbol))
    print("\tavg_sell_price {} {}/ETH\n".format(avg_sell_price,symbol))
    calculate_top_player(today_buys,'seller')
    return today_buys,today_sells

def calculate_stat_hourly(today,daily_buy,daily_sell,hour_start,hour_end,symbol='RDN'):
    today_buys = daily_buy.between_time(hour_start,hour_end)
    today_sells = daily_sell.between_time(hour_start,hour_end)
    if len(today_buys) == 0 or len(today_sells) == 0:
        return
    print("\n\n\t{} Hourly {}-{}\n".format(today,hour_start,hour_end))
    print("**************BUY------------------")
    total_token_buy = today_buys['amount'].sum()
    total_eth_spent = today_buys['amountBase'].sum()
    avg_buy_price = total_eth_spent / total_token_buy


    print("\ttotal_token_buy {} {}".format(total_token_buy,symbol))
    print("\ttotal_eth_spent {} ETH".format(total_eth_spent,symbol))
    print("\tavg_buy_price {} {}/ETH\n".format(avg_buy_price,symbol))

    print("**************SELL------------------")
    total_token_sell = today_sells['amount'].sum()
    total_eth_get = today_sells['amountBase'].sum()
    avg_sell_price = total_eth_get / total_token_sell

    print("\ttotal_token_sell {} {}".format(total_token_sell,symbol))
    print("\ttotal_eth_get {} ETH".format(total_eth_get,symbol))
    print("\ttavg_sell_price {} {}/ETH\n".format(avg_sell_price,symbol))

def main_stat(today):
    today_buys,today_sells = calculate_stat_for_day(today)
    for x in range(1,len(hours)):
        hour_start = hours[x-1]
        hour_curr = hours[x]
        calculate_stat_hourly(today,today_buys,today_sells,hour_start,hour_curr)

def stat_for_an_account(account,symbol='RDN'):
    print("Account STAT {}------------------".format(account))
    account_buys = buys[(buys.buyer == account) | (buys.seller == account)]
    total_token_buy = account_buys['amount'].sum()
    total_eth_spent = account_buys['amountBase'].sum()
    avg_buy_price = total_eth_spent / total_token_buy
    print("------------------BUY------------------")
    print("\ttotal_token_buy {} {}".format(total_token_buy,symbol))
    print("\ttotal_eth_spent {} ETH".format(total_eth_spent,symbol))
    print("\tavg_buy_price {} {}/ETH\n".format(avg_buy_price,symbol))

    account_sells = sells[(sells.buyer == account) | (sells.seller == account)]
    total_token_sell = account_sells['amount'].sum()
    total_eth_get = account_sells['amountBase'].sum()
    avg_sell_price = total_eth_get / total_token_sell
    print("------------------SELL------------------")
    print("\ttotal_token_sell {} {}".format(total_token_sell,symbol))
    print("\ttotal_eth_get {} ETH".format(total_eth_get,symbol))
    print("\ttavg_sell_price {} {}/ETH\n".format(avg_sell_price,symbol))

hours = []
for x in range(0,24):
    hours.append("{}:00".format(x))


# main_stat(today)
# main_stat(tommrow)
stat_for_an_account("0x729dd6c2d977da5380bd7669d66aec3d76852c80")
